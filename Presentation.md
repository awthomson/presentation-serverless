# Intro to Serverless Design
## Secondary Title Here

---

# Introduction

--

## Definition
Wikipedia:
> Serverless computing is a misnomer referring to a cloud-computing execution model in which the cloud provider runs the server, and dynamically manages the allocation of machine resources. Pricing is based on the actual amount of resources consumed by an application, rather than on pre-purchased units of capacity.

--

## Definition
Serverless:
> Leveraging cloud-based services (particularly FaaS, DBs, Storage and Networking) to fully abstract out any OS-level support tasks
Serverless Design:
Designing a solution that aligns to the above (e.g. 

--

## Evolutionary

Continuing trend towards focussing on resource utilisation optimisation
* Physical Servers
* VMs
* Containers
* Functions-as-a-Service (FaaS)

--

## Origins
* Google App Engine in 2008
* AWS Lambdas in 2015
<img src="resources/serverless-trend.png" data-canonical-src="resources/serverless-trend.png" width="800" height="300" />

--

## What that means for us...

* Feedback to developers needs to be as fast as possible (<5 minutes)
* Environment consistency is critical
* Testing becomes integral part of pipeline

---

# Our Pipeline

--

## Reference Process Flow

<img src="resources/Process-Flow.png" data-canonical-src="resources/Process-Flow.png" width="800" height="500" />

---
